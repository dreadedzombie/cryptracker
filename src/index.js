/* STORYBOARD 

INIT CHECK
CHECK FOR LOCATION OF ELECTRUM WALLET FIRST
POPULATE SELECT BOX BASED ON CURRENCIES

ABILITY TO CHOOSE WALLET FILE INCASE DEFAULT_WALLET IS
STORED SOMEWHERE ELSE OR NAME IS DIFFERENT

END STORYBOARD */

const electron = require('electron')
const path = require('path')
const url = require('url')
const shell = require('electron').shell
const BrowserWindow = electron.remote.BrowserWindow

function init() {
  // Minimize task
  document.getElementById("min-btn").addEventListener("click", (e) => {
      var window = BrowserWindow.getFocusedWindow();
      window.minimize();
  });
  document.getElementById("btn-readfile").style.display = 'none';

  // Maximize window
  //document.getElementById("max-btn").addEventListener("click", (e) => {
  //    var window = BrowserWindow.getFocusedWindow();
  //    if(window.isMaximized()){
  //        window.unmaximize();
  //    }else{
  //        window.maximize();
  //    }
  //});

  // Close app
  document.getElementById("close-btn").addEventListener("click", (e) => {
    var window = BrowserWindow.getFocusedWindow();
    window.close();
  });
};

function closeWin(){
    var window = BrowserWindow.getFocusedWindow();
    window.close();
}


function exit() {
  var window = BrowserWindow.getFocusedWindow();
  window.close();
}

function fileMenu(){
  var window = BrowserWindow.getFocusedWindow();
  if(document.getElementById("fileMenu1").value == 'exit'){
    window.close();
  }
  if(document.getElementById("fileMenu1").value == 'open'){
    alert('open');
    document.getElementById("fileMenu1").value = 'file';
  }
  if(document.getElementById("fileMenu2").value == 'copy'){
    document.getElementById("fileMenu2").value = 'edit';
  }
  if(document.getElementById("fileMenu2").value == 'paste'){
    document.getElementById("fileMenu2").value = 'edit';
  }
}

function openFile() {
  const fs = require("fs");
  const {dialog} = require("electron").remote;
  let win = BrowserWindow.getFocusedWindow()
    

  donationCurrency = document.getElementById("donationCurrency")

    if(document.getElementById("donationCurrency").value == 'VIA'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'BTC'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'LTC'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'GRS'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'VTC'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'BCC'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'DASH'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'XMY'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }

    if(document.getElementById("donationCurrency").value == 'XVG'){
      dialog.showOpenDialog((fileNames) => {
        
        if (fileNames === undefined){
          console.log("No files were selected");
          return;
        }
        
        fs.readFile(fileNames.toString(), (err, data) => {
        
        if(err){
            console.log("Cannot read file ", err);
            return;
          }
          if(document.getElementById("donationCurrency").value == 'x'){
          win.setSize(400, 110); 
        } else {
          win.setSize(400, 534); 
        }

        obj = JSON.parse(data.toString());
        document.getElementById('xpub_value').value = obj.accounts[0]['xpub'];// /*JSON.parse(data);*/
        document.getElementById("xpub").style.display = 'block';
        console.log("The content of the file is: ");
        console.log(JSON.parse(data.toString()))
        });
      });
    }
  
}

function pullWallet() {
  const fs = require("fs");
  const remote = require('electron').remote;
  const app = remote.app;

// app.getPath('appData') + 
  const {BrowserWindow} = require('electron').remote
  let win = BrowserWindow.getFocusedWindow()
  if(document.getElementById("donationCurrency").value == 'x'){
    win.setSize(400, 110); 
  } else {
    win.setSize(400, 534); 
  }

  donationCurrency = document.getElementById("donationCurrency")



  if(document.getElementById("donationCurrency").value == 'VIA'){
    fs.readFile(app.getPath('appData') + '/Vialectrum/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Via Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';

      console.log(obj.keystore.xpub);
    });
  }
  
  if(document.getElementById("donationCurrency").value == 'VTC'){
    fs.readFile(app.getPath('appData') + '/Electrum-vtc/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Vertcoin Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'BTC'){
    fs.readFile(app.getPath('appData') + '/Electrum/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Bitcoin Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'LTC'){
    fs.readFile(app.getPath('appData') + '/Electrum-ltc/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Litecoin Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'GRS'){
    fs.readFile(app.getPath('appData') + '/Electrum-GRS/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Groestlcoin Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'BCC'){
    fs.readFile(app.getPath('appData') + '/ElectronCash/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install BitcoinCash ElectronCash wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'DASH'){
    fs.readFile(app.getPath('appData') + '/Electrum-dash/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Dash Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'XMY'){
    fs.readFile(app.getPath('appData') + '/Electrum_myr/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Myriadcoin Electrum wallet! ';
      }
      obj = JSON.parse(data.toString());
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.keystore.xpub;//; /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input id="xpub" value=\"' + obj.keystore.xpub + '\"></input>'); /*JSON.parse(data);*/
      //document.getElementById('result').innerHTML = ('XPUB: </br><input value=\"' + obj.accounts[0]['xpub'] + '\"></input>'); /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.keystore.xpub);
    });
  }

  if(document.getElementById("donationCurrency").value == 'XVG'){
    fs.readFile(app.getPath('appData') + '/Electrum-XVG/wallets/default_wallet', function (err, data) {
      if (err) {
        document.getElementById("xpub").style.display = 'block';
        return document.getElementById('xpub_value').value = 'Does not exist! Please install Verge Electrum wallet! ';
      }
      obj = JSON.parse(data);
      //document.getElementById('result').innerHTML=JSON.parse(data); /*JSON.parse(data);*/
      document.getElementById('xpub_value').value = obj.accounts[0]['xpub'];// /*JSON.parse(data);*/
      document.getElementById("xpub").style.display = 'block';
      console.log(obj.accounts[0]['xpub']);
    });
  }

  if(document.getElementById("donationCurrency").value == 'x'){
        document.getElementById("xpub").style.display = 'none';
  }

};

document.onreadystatechange =  () => {
    if (document.readyState == "complete") {
        init();
        document.getElementById("walletDl").style.display = 'none';
        
    }
};

const cryptips = document.getElementById('cryptips')

function clearCurrency() {
  const {BrowserWindow} = require('electron').remote
  document.getElementById("xpub").style.display = 'none';
  document.getElementById('xpub_value').value = '';

  if(donationCurrency.value == 'x'){
  let win = BrowserWindow.getFocusedWindow();
    win.setSize(400, 110); 
  } else {

  let win = BrowserWindow.getFocusedWindow();
    win.setSize(400, 384);
    document.getElementById("btn-readfile").style.display = 'block';
  }

}

function copyPasta() {
  const el = document.getElementById("xpub_value")
  el.select();
  document.execCommand('copy');
  //document.body.removeChild(el);
}

function showWalletDl() {
  //document.getElementById("mainUI").style.display = 'none';
  //document.getElementById("walletDl").style.display = 'block';
  let wallets = new BrowserWindow({ width: 600, height: 765, frame: false, 'use-content-size': true, opacity: 0.95, transparent: true, maximizable: false})
    wallets.on('close', function() {wallets = null})
    wallets.loadFile('src/wallets.html')
    //win.loadURL('http://cryptips.org/user/dreadedzombie')
    wallets.show()
    //wallets.webContents.openDevTools()
}

function showFAQ() {
  let faq = new BrowserWindow({ width: 600, height: 200, frame: false, 'use-content-size': true, opacity: 0.95, transparent: true, maximizable: false})
    faq.on('close', function() {faq = null})
    faq.loadFile('src/faq.html')
    //win.loadURL('http://cryptips.org/user/dreadedzombie')
    faq.show()
    //faq.webContents.openDevTools()
}

function showAbout() {
  let about = new BrowserWindow({ width: 400, height: 110, frame: false, 'use-content-size': true, opacity: 0.95, transparent: true, maximizable: false})
    about.on('close', function() {faq = null})
    about.loadFile('src/about.html')
    //win.loadURL('http://cryptips.org/user/dreadedzombie')
    about.show()
    //about.webContents.openDevTools()
}

function showDisc() {
  let disclaimer = new BrowserWindow({ width: 400, height: 170, frame: false, 'use-content-size': true, opacity: 0.95, transparent: true, maximizable: false})
    disclaimer.on('close', function() {faq = null})
    disclaimer.loadFile('src/disclaimer.html')
    //win.loadURL('http://cryptips.org/user/dreadedzombie')
    disclaimer.show()
    //disclaimer.webContents.openDevTools()
}

function rollUp() {
  const {BrowserWindow} = require('electron').remote
  let win = BrowserWindow.getFocusedWindow()
  win.setSize(400, 110); 
}

function openBrowser() {
  //open links externally by default
  $(document).on('click', 'a[href^="http"]', function(event) {
      event.preventDefault();
      shell.openExternal(this.href);
  });
}



// API STUFF

function etherscan_api() {
  const https = require('https');

  https.get('https://chainz.cryptoid.info/grs/api.dws?q=getbalance&a='+'test', (resp) => {
  //https.get('https://chainz.cryptoid.info/grs/api.dws?q=getbalance&a=', (resp) => {

    let data = '';

    // A chunk of data has been recieved.
    resp.on('data', (chunk) => {
      data += chunk;
    });
1
    // The whole response has been received. Print out the result.
    resp.on('end', () => {
      console.log(JSON.parse(data));
      document.getElementById('result').innerHTML=JSON.parse(data);
    });

  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
};

