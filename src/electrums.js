function electrumBTC(){
  const ElectrumCli = require('electrum-client')
const main = async () => {
    const ecl = new ElectrumCli(995, 'btc.smsys.me', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("2.7.11", "1.0"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);

    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}

function electrumGRS(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50002, 'electrum15.groestlcoin.org', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("0", "1.2"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}

function electrumLTC(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50004, 'electrum-ltc0.snel.it', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("0", "1.2"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}


function electrumVTC(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(55002, 'fr1.vtconline.org', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("0", "1.2"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}

function electrumBCC(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50002, 'electroncash.checksum0.com', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("0", "1.2"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}

function electrumDASH(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50002, 'electrum.dash.siampm.com', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("0", "1.2"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}

function electrumXMY(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50004, 'cetus.cryptap.us', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version("0", "1.2"); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(e)
    }
    await ecl.close() // disconnect(promise)
}
main()
}


function electrumXVG(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50002, 'e3.verge-electrum.com', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version(); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(ver)
    }
    await ecl.close() // disconnect(promise)
}
main()
}

function electrumVIA(){
  const ElectrumCli = require('electrum-client')
  const main = async () => {
    const ecl = new ElectrumCli(50002, 'viax2.bitops.me', 'tls') // tcp or tls
    await ecl.connect() // connect(promise)
    ecl.subscribe.on('blockchain.headers.subscribe', (v) => console.log(v)) // subscribe message(EventEmitter)
    try{
        const ver = await ecl.server_version(); // json-rpc(promise)
        console.log(ver);
        document.getElementById('print').innerHTML = (ver);
    }catch(e){
        console.log(ver)
    }
    await ecl.close() // disconnect(promise)
}
main()
}


function test() {
  const ElectrumCli = require('electrum-client')

	const sleep = (ms) => new Promise((resolve,_) => setTimeout(() => resolve(), ms))

	const main = async () => {
	    try{
	        const ecl = new ElectrumCli(995, 'btc.smsys.me', 'tls')
	        ecl.subscribe.on('server.peers.subscribe', console.log)
	        ecl.subscribe.on('blockchain.numblocks.subscribe', console.log)
	        ecl.subscribe.on('blockchain.headers.subscribe', console.log)
	        ecl.subscribe.on('blockchain.address.subscribe', console.log)
	        ecl.subscribe.on('blockchain.scripthash.subscribe', console.log)
	        ecl.subscribe.on('blockchain.address.get_balance', console.log)
	        ecl.subscribe.on('blockchain.address.get_history', console.log)
	        await ecl.connect()
	        await ecl.server_version("0", "1.2")
	        const p1 = await ecl.serverPeers_subscribe()
	        const p2 = await ecl.blockchainHeaders_subscribe()
	        // Note: blockchain.numblocks.subscribe is deprecated in protocol version 1.1
	        const p3 = await ecl.blockchainAddress_subscribe('12c6DSiU4Rq3P4ZxziKxzrL5LmMBrzjrJX')
	        // Subscribe to corresponding scripthash for the above address
	        const p4 = await ecl.blockchainScripthash_subscribe('f91d0a8a78462bc59398f2c5d7a84fcff491c26ba54c4833478b202796c8aafd')
	        const p5 = await ecl.blockchainAddress_getBalance('12c6DSiU4Rq3P4ZxziKxzrL5LmMBrzjrJX')
	        const p6 = await ecl.blockchainAddress_getHistory('12c6DSiU4Rq3P4ZxziKxzrL5LmMBrzjrJX')

	        console.log(p1, "111111111")
	        console.log(p2, "222222222")
	        console.log(p3, "333333333")
	        console.log(p4, "444444444")
             calcBTC = JSON.stringify(p5.confirmed) / 10 ** 8
	        console.log(calcBTC)
	         // 10 ** 18, "555555555"
	        console.log(p6, "666666666")
            btcAdrdress = document.getElementById('btcAddress').value;

            const getBalance = await ecl.blockchainAddress_getBalance(btcAddress)

            document.getElementById('print').innerHTML = (calcBTC);
            document.getElementById('print').innerHTML = (calcBTC);

	        while(true){
	            await sleep(1000)
	            const ver = await ecl.server_version("0", "1.2")
	        }
	        await ecl.close()
	    }catch(e){
	        console.log("error")
	        console.log(e)
	    }
	}
	main()	
}


function btcGrab() {

      const ElectrumCli = require('electrum-client')

    const sleep = (ms) => new Promise((resolve,_) => setTimeout(() => resolve(), ms))

    const main = async () => {
        try{
            const ecl = new ElectrumCli(995, 'btc.smsys.me', 'tls')
            ecl.subscribe.on('server.peers.subscribe', console.log)
            ecl.subscribe.on('blockchain.numblocks.subscribe', console.log)
            ecl.subscribe.on('blockchain.headers.subscribe', console.log)
            ecl.subscribe.on('blockchain.address.subscribe', console.log)
            ecl.subscribe.on('blockchain.scripthash.subscribe', console.log)
            ecl.subscribe.on('blockchain.address.get_balance', console.log)
            ecl.subscribe.on('blockchain.address.get_history', console.log)
            await ecl.connect()
            await ecl.server_version("0", "1.2")
            // Note: blockchain.numblocks.subscribe is deprecated in protocol version 1.1
            // Subscribe to corresponding scripthash for the above address
            //const p5 = await ecl.blockchainAddress_getBalance('12c6DSiU4Rq3P4ZxziKxzrL5LmMBrzjrJX')

            //calcBTC = JSON.stringify(p5.confirmed) / 10 ** 8
            //console.log(calcBTC)
             // 10 ** 18, "555555555"s
            btcAddress = document.getElementById('btcAddress').value;
            console.log(btcAddress)
            const getBalance = await ecl.blockchainAddress_getBalance(btcAddress)
            const getHistory = await ecl.blockchainAddress_getHistory(btcAddress)
            //console.log(getBalance)
            //console.log(getHistory)

            Object.keys(getHistory).forEach(function(k){
                console.log(k + ' - ' + JSON.stringify(getHistory[k].tx_hash));
                document.getElementById('history').innerHTML = "<div>" + JSON.stringify(getHistory[k].tx_hash) + "</div>"; 

            });
            document.getElementById('print').innerHTML = (getBalance.confirmed / 10 ** 8);
            document.getElementById('history').innerHTML = (JSON.stringify(getHistory));
            //document.getElementById('history').innerHTML = "<div>" + JSON.stringify(getHistory[k].tx_hash) + "</div>"; 

            while(true){
                await sleep(1000)
                const ver = await ecl.server_version("0", "1.2")
            }
            await ecl.close()
        }catch(e){
            console.log("error")
            console.log(e)
        }
    }
    main()  
}

function grsGrab() {

      const ElectrumCli = require('electrum-client')

    const sleep = (ms) => new Promise((resolve,_) => setTimeout(() => resolve(), ms))

    const main = async () => {
        try{
            const ecl = new ElectrumCli(50002, 'electrum15.groestlcoin.org', 'tls')
            ecl.subscribe.on('server.peers.subscribe', console.log)
            ecl.subscribe.on('blockchain.numblocks.subscribe', console.log)
            ecl.subscribe.on('blockchain.headers.subscribe', console.log)
            ecl.subscribe.on('blockchain.address.subscribe', console.log)
            ecl.subscribe.on('blockchain.scripthash.subscribe', console.log)
            ecl.subscribe.on('blockchain.address.get_balance', console.log)
            ecl.subscribe.on('blockchain.address.get_history', console.log)
            await ecl.connect()
            await ecl.server_version("0", "1.2")
            // Note: blockchain.numblocks.subscribe is deprecated in protocol version 1.1
            // Subscribe to corresponding scripthash for the above address
            //const p5 = await ecl.blockchainAddress_getBalance('12c6DSiU4Rq3P4ZxziKxzrL5LmMBrzjrJX')

            //calcBTC = JSON.stringify(p5.confirmed) / 10 ** 8
            //console.log(calcBTC)
             // 10 ** 18, "555555555"s
            grsAddress = document.getElementById('grsAddress').value;
            console.log(grsAddress)
            const getBalance = await ecl.blockchainAddress_getBalance(grsAddress)
            const getHistory = await ecl.blockchainAddress_getHistory(grsAddress)
            //console.log(getBalance)
            //console.log(getHistory)

            Object.keys(getHistory).forEach(function(k){
                console.log(k + ' - ' + JSON.stringify(getHistory[k].tx_hash));
                document.getElementById('history').innerHTML = "<div>" + JSON.stringify(getHistory[k].tx_hash) + "</div>"; 

            });
            document.getElementById('print').innerHTML = (getBalance.confirmed / 10 ** 8);
            document.getElementById('history').innerHTML = (JSON.stringify(getHistory));
            //document.getElementById('history').innerHTML = "<div>" + JSON.stringify(getHistory[k].tx_hash) + "</div>"; 

            while(true){
                await sleep(1000)
                const ver = await ecl.server_version("0", "1.2")
            }
            await ecl.close()
        }catch(e){
            console.log("error")
            console.log(e)
        }
    }
    main()  
}